# MADB Duty CSV Exporter

CSV exporter to get data from the https://madb.europa.eu/madb/datasetPreviewFormATpubli.htm?datacat_id=AT&from=publi service.
The script has been intended to extract the same set of codes for each nation.

# How to run the scritp

To run the exporter run the script as

```
./madbDutyCSVExporter
```
 or on Windows OS
 ```
 madbDutyCSVExporter.exe
 ```

Each run regenerates the file `result.csv` composed by these columns:

| Country | Code | GEN | MFN | Description |
| ---     |  --- | --- | --- | ---         |
| IN | 8420.10 | 7.5% | | - Calendering or other rolling machines |
| IN | 8420.91 | 7.5% | | - - Cylinders |
| IN | 8420.99 | 7.5% | | - - Other |
| IN | 8421.11 | 7.5% | | - - Cream separators |
| IN | 8421.12 | 7.5% | | - - Clothes dryers |

# How change Country and Code

To change from which country or code you want to fetch data change the ```properties.conf``` file.
Remember that the code needs to be 4 digit.

```conf
! Values fot extraction

nations=BR;CN;US;IN
codes=8422;8421;8420
```
