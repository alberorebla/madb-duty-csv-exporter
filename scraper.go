package main

import (
	"encoding/csv"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/magiconair/properties"
)

// https://madb.europa.eu/madb/atDutyOverviewPubli.htm?countries=BR&hscode=8421

func callSvc(country, code string) [][]string {

	var customURL url.URL
	customURL.Scheme = "https"
	customURL.Host = "madb.europa.eu"
	customURL.Path = "madb/atDutyOverviewPubli.htm"
	newQueryValues := customURL.Query()
	newQueryValues.Set("countries", country)
	newQueryValues.Set("hscode", code)

	customURL.RawQuery = newQueryValues.Encode()
	response, err := http.Get(customURL.String())

	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}
	// ("#tarrifsTable > tbody > tr > td:nth-child(1) > a")

	resultset := [][]string{}

	document.Find("#tarrifsTable > tbody > tr").Each(func(index int, element *goquery.Selection) {
		// See if the href attribute exists on the element
		coden := strings.TrimSpace(element.Find("td:nth-child(1)").Text())
		description := strings.TrimSpace(element.Find("td:nth-child(2)").Text())
		gen := strings.TrimSpace(element.Find("td:nth-child(3)").Text())
		mfn := strings.TrimSpace(element.Find("td:nth-child(4)").Text())

		if len(coden) > 0 && len(gen) > 0 {
			resultset = append(resultset, []string{country, coden, gen, mfn, description})
		}
	})

	return resultset
}

func main() {

	p := properties.MustLoadFile("properties.conf", properties.UTF8)

	resultset := [][]string{}

	type Config struct {
		Nations []string `properties:"nations"`
		Codes   []string `properties:"codes"`
	}
	var cfg Config

	if err := p.Decode(&cfg); err != nil {
		log.Fatal(err)
	}

	for _, nation := range cfg.Nations {
		for _, code := range cfg.Codes {
			resultset = append(callSvc(nation, code), resultset...)
		}
	}

	file, err := os.Create("result.csv")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range resultset {
		err := writer.Write(value)
		if err != nil {
			log.Fatal("Cannot write to file", err)
		}
	}
}
